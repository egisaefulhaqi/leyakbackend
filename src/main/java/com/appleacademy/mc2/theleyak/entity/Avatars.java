/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Entity
@Table(name = "avatars")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Avatars.findAll", query = "SELECT a FROM Avatars a")
    , @NamedQuery(name = "Avatars.findByAvatarId", query = "SELECT a FROM Avatars a WHERE a.avatarId = :avatarId")
    , @NamedQuery(name = "Avatars.findByAvatarName", query = "SELECT a FROM Avatars a WHERE a.avatarName = :avatarName")
    , @NamedQuery(name = "Avatars.findByAvatarSource", query = "SELECT a FROM Avatars a WHERE a.avatarSource = :avatarSource")})
public class Avatars implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "avatar_id")
    private Integer avatarId;
    @Column(name = "avatar_name")
    private String avatarName;
    @Column(name = "avatar_source")
    private String avatarSource;

    public Avatars() {
    }

    public Avatars(Integer avatarId) {
        this.avatarId = avatarId;
    }

    public Integer getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(Integer avatarId) {
        this.avatarId = avatarId;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    public String getAvatarSource() {
        return avatarSource;
    }

    public void setAvatarSource(String avatarSource) {
        this.avatarSource = avatarSource;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avatarId != null ? avatarId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Avatars)) {
            return false;
        }
        Avatars other = (Avatars) object;
        if ((this.avatarId == null && other.avatarId != null) || (this.avatarId != null && !this.avatarId.equals(other.avatarId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.appleacademy.mc2.entity.Avatars[ avatarId=" + avatarId + " ]";
    }
    
}
