/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Entity
@Table(name = "lobby")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lobby.findAll", query = "SELECT l FROM Lobby l")
    , @NamedQuery(name = "Lobby.findByLobbyId", query = "SELECT l FROM Lobby l WHERE l.lobbyId = :lobbyId")
    , @NamedQuery(name = "Lobby.findByLobbyName", query = "SELECT l FROM Lobby l WHERE l.lobbyName = :lobbyName")
    , @NamedQuery(name = "Lobby.findByUserHost", query = "SELECT l FROM Lobby l join l.userHost u WHERE u.userId = :userId")
    , @NamedQuery(name = "Lobby.findByStatus", query = "SELECT l FROM Lobby l WHERE l.status = :status")
    , @NamedQuery(name = "Lobby.findByType", query = "SELECT l FROM Lobby l WHERE l.type = :type")
    , @NamedQuery(name = "Lobby.findByPasskey", query = "SELECT l FROM Lobby l WHERE l.passkey = :passkey")})
public class Lobby implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "lobby_id")
    private Integer lobbyId;
    @Column(name = "lobby_name")
    private String lobbyName;
    @JoinColumn(name = "user_host", referencedColumnName = "user_id")
    @ManyToOne
    private Users userHost;
    @Column(name = "status")
    private String status;
    @Column(name = "type")
    private String type;
    @Column(name = "passkey")
    private String passkey;

    public Lobby() {
    }

    public Lobby(Integer lobbyId) {
        this.lobbyId = lobbyId;
    }

    public Integer getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(Integer lobbyId) {
        this.lobbyId = lobbyId;
    }

    public String getLobbyName() {
        return lobbyName;
    }

    public void setLobbyName(String lobbyName) {
        this.lobbyName = lobbyName;
    }

    public Users getUserHost() {
        return userHost;
    }

    public void setUserHost(Users userHost) {
        this.userHost = userHost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPasskey() {
        return passkey;
    }

    public void setPasskey(String passkey) {
        this.passkey = passkey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lobbyId != null ? lobbyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lobby)) {
            return false;
        }
        Lobby other = (Lobby) object;
        if ((this.lobbyId == null && other.lobbyId != null) || (this.lobbyId != null && !this.lobbyId.equals(other.lobbyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.appleacademy.mc2.entity.Lobby[ lobbyId=" + lobbyId + " ]";
    }
    
}
