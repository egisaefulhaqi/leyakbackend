/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Users;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.log4j.Logger;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Transactional
@Service
public class UsersServiceImpl implements UsersService{
    private static final Logger log = Logger.getLogger(UsersServiceImpl.class);
    @Autowired
    private EntityManager em;
    @Override
    public void create(Users user) {
        try {
            em.persist(user);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        
    }

    @Override
    public void edit(Users user) {
        try {
            em.merge(user);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Users find(Object id) {
        try {
            return em.find(Users.class, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
        
    }

    @Override
    public Users loginAttempt(String username) {
        String query = "select u from Users u join u.avatar a where u.username = :username";
        try {
            return (Users) em.createQuery(query).setParameter("username", username).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            //log.error(e.getMessage());
            System.out.println("Error : "+e.getMessage());
        }
        return null;
    }
    
}
