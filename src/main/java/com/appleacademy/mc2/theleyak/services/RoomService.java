/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Rooms;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public interface RoomService {
    void create(Rooms room);
    
    void edit(Rooms item);
    
    public Rooms find(Object id);
    
}
