/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Users;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public interface UsersService {
    void create(Users user);
    
    void edit(Users user);
    
    public Users find(Object id);
    
    public Users loginAttempt(String username);
            
    
}
