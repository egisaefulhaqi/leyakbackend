/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Avatars;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public interface AvatarService {
    
    void create(Avatars avatar);
    
    void edit(Avatars avatar);
    
    public Avatars find(Object id);
    
    
}
