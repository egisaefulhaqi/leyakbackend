/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Rooms;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Transactional
@Service
public class RoomServiceImpl implements RoomService{
    
    private static final Logger log = Logger.getLogger(RoomServiceImpl.class);
    @Autowired
    private EntityManager em;
    
    @Override
    public void create(Rooms room) {
        try {
            em.persist(room);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void edit(Rooms item) {
        try {
            em.merge(item);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Rooms find(Object id) {
        try {
            return em.find(Rooms.class, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
    
}
