/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Lobby;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Transactional
@Service
public class LobbyServiceImpl implements LobbyService{

    private static final Logger log = Logger.getLogger(LobbyServiceImpl.class);
    @Autowired
    private EntityManager em;
    
    @Override
    public void create(Lobby lobby) {
        try {
            em.persist(lobby);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void edit(Lobby lobby) {
        try {
            em.merge(lobby);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Lobby find(Object id) {
        try {
            return em.find(Lobby.class, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Lobby> findByNetwork(String network) {
        String query = "select l from Lobby l join l.userHost u where u.network = :network and l.status = 'A'";
        try {
            
            return (List<Lobby>) em.createQuery(query).setParameter("network", network).getResultList();
            
        } catch (Exception e) {
            log.error(e);
        }
        return null;
    }
    
}
