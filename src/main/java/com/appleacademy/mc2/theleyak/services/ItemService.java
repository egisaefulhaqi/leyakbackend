/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Items;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public interface ItemService {
    
    void create(Items item);
    
    void edit(Items item);
    
    public Items find(Object id);
    
}
