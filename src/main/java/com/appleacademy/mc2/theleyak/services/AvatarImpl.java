/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Avatars;
import com.appleacademy.mc2.theleyak.entity.Users;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@Transactional
@Service
public class AvatarImpl implements AvatarService{

    private static final Logger log = Logger.getLogger(AvatarImpl.class);
    @Autowired
    private EntityManager em;
    
    @Override
    public void create(Avatars avatar) {
        try {
            em.persist(avatar);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void edit(Avatars avatar) {
        try {
            em.merge(avatar);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Avatars find(Object id) {
        try {
            return em.find(Avatars.class, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
    
}
