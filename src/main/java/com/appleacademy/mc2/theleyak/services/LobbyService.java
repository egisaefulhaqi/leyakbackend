/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Lobby;
import java.util.List;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public interface LobbyService {
    
    void create(Lobby lobby);
    
    void edit(Lobby lobby);
    
    public Lobby find(Object id);
    
    public List<Lobby> findByNetwork(String network);
    
    
}
