/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.services;

import com.appleacademy.mc2.theleyak.entity.Items;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
public class ItemServiceImpl implements ItemService{
    
    private static final Logger log = Logger.getLogger(ItemServiceImpl.class);
    @Autowired
    private EntityManager em;

    @Override
    public void create(Items item) {
        try {
            em.persist(item);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void edit(Items item) {
        try {
            em.merge(item);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Items find(Object id) {
        try {
            return em.find(Items.class, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }
    
}
