/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.controller;

import com.appleacademy.mc2.theleyak.entity.Lobby;
import com.appleacademy.mc2.theleyak.entity.Users;
import com.appleacademy.mc2.theleyak.services.LobbyService;
import com.appleacademy.mc2.theleyak.services.UsersService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@RestController
@RequestMapping("TheLeyak")
public class LobbyController {

    private static final Logger log = Logger.getLogger(LobbyController.class);

    @Autowired
    private UsersService userService;
    @Autowired
    private LobbyService lobbyService;

    @RequestMapping(value = "/LobbyCreate", method = RequestMethod.POST)
    public String createLobby(HttpServletRequest request) throws Exception {
        JSONObject result = new JSONObject();
        try {
            String username = request.getParameter("username");
            if (username == null || username.equalsIgnoreCase("")) {
                result.put("response", "Username cant be null");
                result.put("responseCode", "0100");
                return result.toString();
            }
            String networkSSID = request.getParameter("network");
            if (networkSSID == null || networkSSID.equalsIgnoreCase("")) {
                result.put("response", "networkSSID cant be null");
                result.put("responseCode", "0101");
                return result.toString();
            }
            String lobbyName = request.getParameter("lobbyName");
            if (lobbyName == null || lobbyName.equalsIgnoreCase("")) {
                result.put("response", "Lobby Name cant be null");
                result.put("responseCode", "0103");
                return result.toString();
            }
            String lobbyType = request.getParameter("lobbyType");
            if (lobbyType == null || lobbyType.equalsIgnoreCase("")) {
                result.put("response", "Lobby Type cant be null");
                result.put("responseCode", "0105");
                return result.toString();
            }

            Users user = userService.loginAttempt(username);
            if (user == null) {
                result.put("response", "User Not Found");
                result.put("responseCode", "0107");
                return result.toString();
            }

            Lobby lobby = new Lobby();
            lobby.setLobbyName(lobbyName);
            lobby.setStatus("A");
            lobby.setType(lobbyType);
            lobby.setUserHost(user);
            lobby.setPasskey(request.getParameter("passkey"));

            lobbyService.create(lobby);

            result.put("response", "Success");
            result.put("responseCode", "0000");

        } catch (Exception e) {
            result.put("response", "Unexpected Error");
            result.put("responseCode", "9999");
        }
        return result.toString();
    }

    @RequestMapping(value = "/LobbyList", method = RequestMethod.POST)
    public String listLobby(HttpServletRequest request) throws Exception {
        JSONObject result = new JSONObject();
        try {
            String networkSSID = request.getParameter("network");
            if (networkSSID == null || networkSSID.equalsIgnoreCase("")) {
                result.put("response", "networkSSID cant be null");
                result.put("responseCode", "0101");
                return result.toString();
            }

            List<Lobby> lobby = lobbyService.findByNetwork(networkSSID);
            Map data = new HashMap<String, String>();
            if (lobby.size() > 0) {
               
                for (Lobby lobbes : lobby) {
                    data.put("lobbyName", lobbes.getLobbyName());
                    data.put("lobbyType", lobbes.getType());
                    data.put("passkey", lobbes.getPasskey());
                    data.put("hosts", lobbes.getUserHost().getUsername());
                    
                }
                result.put("data", data);
            }

            result.put("response", "Success");
            result.put("responseCode", "0000");
            result.put("lobbyFound", lobby.size());

        } catch (Exception e) {
            result.put("response", "Unexpected Error");
            result.put("responseCode", "9999");
        }
        return result.toString();
    }

}
