/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appleacademy.mc2.theleyak.controller;

import com.appleacademy.mc2.theleyak.entity.Users;
import com.appleacademy.mc2.theleyak.services.AvatarService;
import com.appleacademy.mc2.theleyak.services.UsersService;
import com.appleacademy.mc2.theleyak.services.UsersServiceImpl;
import java.util.Date;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author egimuhamadsaefulhaqi
 */
@RestController
@RequestMapping("TheLeyak")
public class UserController {

    private static final Logger log = Logger.getLogger(UsersServiceImpl.class);

    @Autowired
    private UsersService userService;
    @Autowired
    private AvatarService avaService;

    @RequestMapping(value = "/Login", method = RequestMethod.POST)
    public String login(HttpServletRequest request) throws Exception {
        JSONObject result = new JSONObject();
        try {
            String username = request.getParameter("username");
            if (username == null || username.equalsIgnoreCase("")) {
                result.put("response", "Username cant be null");
                result.put("responseCode", "0100");
                return result.toString();
            }
            String networkSSID = request.getParameter("network");
            if (networkSSID == null || networkSSID.equalsIgnoreCase("")) {
                result.put("response", "networkSSID cant be null");
                result.put("responseCode", "0101");
                return result.toString();
            }

            Users u = userService.loginAttempt(username);

            if (u == null) {
                Users newUser = new Users();
                newUser.setLastLogin(new Date());
                newUser.setNetwork(networkSSID);
                newUser.setStatus("A");
                newUser.setAvatar(avaService.find(1));
                newUser.setUsername(username);
                newUser.setDevice(request.getParameter("device"));
                userService.create(newUser);
                result.put("response", "SUCCESS");
                result.put("responseCode", "0000");
                result.put("username", newUser.getUsername());
                result.put("avatar", newUser.getAvatar().getAvatarSource());
                result.put("status", newUser.getStatus());
                result.put("device", newUser.getDevice());
                result.put("network", newUser.getNetwork());
                return result.toString();
            }
            u.setLastLogin(new Date());
            u.setNetwork(networkSSID);
            u.setStatus("A");
            //Edit data
            userService.edit(u);

            result.put("response", "SUCCESS");
            result.put("responseCode", "0000");
            result.put("username", u.getUsername());
            result.put("avatar", u.getAvatar().getAvatarSource());
            result.put("status", u.getStatus());
            result.put("device", u.getDevice());
            result.put("network", u.getNetwork());

        } catch (Exception e) {
            e.printStackTrace();
            result.put("responseMsg", "Unexpected Error");
            result.put("responseCode", "9999");
        }
        return result.toString();

    }

}
