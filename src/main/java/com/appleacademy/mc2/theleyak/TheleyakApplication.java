package com.appleacademy.mc2.theleyak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheleyakApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheleyakApplication.class, args);
	}
}
